using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatalogueListItem : MonoBehaviour
{
    [SerializeField] private Text ItemNameText;
    [Space(10)]
    [SerializeField] private Text ItemDescriptionText;
    [SerializeField] private Image ItemIcon;

    public void Init(CatalogItem catalogItem, CatalogConfig catalogConfig)
    {
        ItemNameText.text = catalogItem.DisplayName;
        ItemDescriptionText.text = catalogItem.Description;
        ItemIcon.sprite = catalogConfig[catalogItem.ItemId];
    }
}
