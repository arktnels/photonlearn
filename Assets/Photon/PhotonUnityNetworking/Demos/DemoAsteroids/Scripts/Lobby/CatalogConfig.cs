using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "CatalogConfig", menuName = "PlayFab/Catalog icon config")]
public class CatalogConfig : ScriptableObject
{
    public List<ConfigPair> configPairs;

    public Sprite this[string name] =>
        configPairs.Where(pair => pair.name.Equals(name)).FirstOrDefault().icon;
}

[System.Serializable]
public struct ConfigPair
{
    public string name;
    public Sprite icon;
}
