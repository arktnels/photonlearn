using PlayFab;
using PlayFab.ClientModels;
using PlayFab.MultiplayerModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;
using UnityEngine.UI;

public enum TypeCharacter
{
    Green,
    Blue
}

public class CharacterStoreItem : MonoBehaviour
{
    [SerializeField] private TypeCharacter type;
    [SerializeField] private Button _characterButton;

    [SerializeField] private GameObject _enabledPortrait;
    [SerializeField] private Image _outlinePortrait;
    [SerializeField] private Text _nameCharacter;
    [SerializeField] private Text _levelCharacter;

    public TypeCharacter Type => type;

    public void SetCharacter(CharacterResult character, Action<TypeCharacter> buyAction)
    {
        _enabledPortrait.SetActive(true);
        _nameCharacter.text = character.CharacterName;
        UpdateCharacterStatistic(character.CharacterId);
        _characterButton.onClick.RemoveAllListeners();
        _characterButton.onClick.AddListener(() => buyAction(type));
    }

    public void SetEmptySlot(Action<TypeCharacter> buyAction)
    {
        _enabledPortrait.SetActive(false);
        _characterButton.onClick.RemoveAllListeners();
        _characterButton.onClick.AddListener(() => buyAction(type));
    }

    private void UpdateCharacterStatistic(string characterId)
    {
        PlayFabClientAPI.GetCharacterStatistics(new GetCharacterStatisticsRequest
        {
            CharacterId = characterId
        }, result =>
        {
            _levelCharacter.text = result.CharacterStatistics["Level"].ToString();
        }, Debug.LogError);
    }

    public void Selected(bool isSelected)
    {
        _outlinePortrait.color = isSelected ? Color.green : Color.white;
    }
}
