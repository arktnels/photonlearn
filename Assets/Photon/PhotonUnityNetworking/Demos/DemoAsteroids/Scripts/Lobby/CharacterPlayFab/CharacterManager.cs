using PlayFab.ClientModels;
using PlayFab;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CharacterManager : MonoBehaviour
{
    private const string BLUE_ID = "char_blue";
    private const string GREEN_ID = "char_green";
    private const string CURRENCY = "SL";

    public static TypeCharacter SelectedCharacter { get; private set; }

    [SerializeField] private CharacterStoreItem[] _characterItems;
    public Action<List<CharacterResult>> refreshCharacters;

    private List<CharacterResult> _characters = new List<CharacterResult>();
    
    void Start()
    {
        //GetCharacters();
    }

    public void GetCharacters()
    {
        PlayFabClientAPI.GetAllUsersCharacters(new ListUsersCharactersRequest(), result =>
            {
                Debug.Log($"Characters owned: + {result.Characters.Count}");
                if (_characters.Count > 0)
                {
                    _characters.Clear();
                }
                foreach (var character in _characterItems)
                    character.SetEmptySlot(BuyCharacter);
                foreach (var characterResult in result.Characters)
                {
                    _characters.Add(characterResult);
                    switch (characterResult.CharacterType)
                    {
                        case BLUE_ID:
                            _characterItems
                                .Where(item => item.Type == TypeCharacter.Blue)
                                .FirstOrDefault()
                                .SetCharacter(characterResult, SelectCharacter);
                            break;
                        case GREEN_ID:
                            _characterItems
                                .Where(item => item.Type == TypeCharacter.Green)
                                .FirstOrDefault()
                                .SetCharacter(characterResult, SelectCharacter);
                            break;
                    }
                }

                //refreshCharacters?.Invoke(_characters);
            }, Debug.LogError);
    }

    private void SelectCharacter(TypeCharacter type)
    {
        SelectedCharacter = type;
        foreach (var character in _characterItems)
            character.Selected(false);
        switch (SelectedCharacter)
        {
            case TypeCharacter.Green:
                _characterItems
                    .Where(item => item.Type == TypeCharacter.Green)
                    .FirstOrDefault()
                    .Selected(true);
                break;
            case TypeCharacter.Blue:
                _characterItems
                    .Where(item => item.Type == TypeCharacter.Blue)
                    .FirstOrDefault()
                    .Selected(true);
                break;
        }
    }

    private void BuyCharacter(TypeCharacter type)
    {
        PlayFabClientAPI.GetStoreItems(new GetStoreItemsRequest
        {
            CatalogVersion = "Char_1",  //���� ID ��������
            StoreId = "char_st"         //���� ID ��������
        }, result =>
        {
            var id = type switch
            {
                TypeCharacter.Blue => BLUE_ID,
                TypeCharacter.Green => GREEN_ID,
                _ => BLUE_ID
            };
            CreateNewCharacter(result.Store.Where(item => item.ItemId == id).FirstOrDefault(), type);
        }, Debug.LogError);
    }

    private void CreateNewCharacter(StoreItem item, TypeCharacter type)
    {
        PlayFabClientAPI.PurchaseItem(new PurchaseItemRequest
        {
            CatalogVersion = "Char_1",
            ItemId = item.ItemId,
            Price = (int)item.VirtualCurrencyPrices[CURRENCY],
            VirtualCurrency = CURRENCY
        }, result =>
        {
            foreach (var item in result.Items)
            {
                CreateCharacterWithItemId(item.ItemId, type);
                return;
            }
        }, Debug.LogError);
    }

    private void CreateCharacterWithItemId(string itemId, TypeCharacter type)
    {
        PlayFabClientAPI.GrantCharacterToUser(new GrantCharacterToUserRequest
        {
            CatalogVersion = "Char_1",
            CharacterName = type switch
            {
                TypeCharacter.Blue => "Blue Moose",
                TypeCharacter.Green => "Green Moose",
                _ => "Moose"
            },
            ItemId = itemId
        }, result =>
        {
            Debug.Log($"Get character type: {result.CharacterType}");
            UpdateCharacterStatistics(result.CharacterId);
        }, Debug.LogError);
    }

    private void UpdateCharacterStatistics(string characterId)
    {
        PlayFabClientAPI.UpdateCharacterStatistics(new UpdateCharacterStatisticsRequest
        {
            CharacterId = characterId,
            CharacterStatistics = new Dictionary<string, int>
            {
                {"Level", 1},
                {"Exp", 0},
                {"Gold", 0}
            }
        }, result =>
        {
            GetCharacters();
        }, Debug.LogError);
    }
}
