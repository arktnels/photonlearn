using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _playerBluePrefab;
    [SerializeField] private GameObject _playerGreenPrefab;

    void Start()
    {
        var name = CharacterManager.SelectedCharacter switch
        {
            TypeCharacter.Green => _playerGreenPrefab.name,
            TypeCharacter.Blue => _playerBluePrefab.name,
            _ => _playerBluePrefab.name,
        };
        PhotonNetwork.Instantiate(name, transform.position, transform.rotation);
    }
}
