using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PhotonLobbyManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private TMP_InputField _playerName;
    [SerializeField] private TMP_InputField _roomName;

    [SerializeField] private Button _createRoomButton;

    private void Awake()
    {
        _createRoomButton.onClick.AddListener(OnCreatedRoom);
    }

    private void OnCreateRoom()
    {
        PhotonNetwork.LocalPlayer.NickName = _playerName.text;
        string roomName = _roomName.text;
        roomName = (roomName.Equals(string.Empty)) ? "Room " + Random.Range(1000, 10000) : roomName;

        byte maxPlayers = 4;

        RoomOptions options = new RoomOptions { MaxPlayers = maxPlayers, PlayerTtl = 10000 };
        PhotonNetwork.JoinRandomOrCreateRoom(roomName: roomName, roomOptions: options);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log($"You jouned room: {PhotonNetwork.CurrentRoom.Name}");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        Debug.LogError(message);
    }
}
